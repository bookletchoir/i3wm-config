#!/usr/bin/env bash

src_conf="${PWD}/conf"
dest_conf="${XDG_CONFIG_HOME:=${HOME}/.config}/i3"
dest_conf_bak="${dest_conf}_bak"

src_blocklets="${PWD}/src"
dest_blocklets="${dest_conf}/blocklets"

# create backup and remove target dir

if [[ -d ${dest_conf} ]]; then
    if [[ ! -d ${dest_conf_bak} ]]; then
        mv "${dest_conf}" "${dest_conf_bak}"
        echo mv "${dest_conf}" "${dest_conf_bak}"
    fi

    rm -rf "${dest_conf}"
    echo rm -rf "${dest_conf}"
fi

# copy new configs over

cp -r "${src_conf}" "${dest_conf}"
echo cp -r "${src_conf}" "${dest_conf}"

cp -r "${src_blocklets}" "${dest_blocklets}"
echo cp -r "${src_blocklets}" "${dest_blocklets}"
