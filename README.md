i3wm config with [i3blocks](https://github.com/vivien/i3blocks).

Uses [OhSnap](https://sourceforge.net/projects/osnapfont/) font.

Optionally required nvidia-utils (`nvidia-smi`), NetworkManager (`nmcli`) and `mpd` + `mpc`.

i3wm + i3blocks configs: Edit `conf/{config,i3blocks.conf}` files

Blocklets config: Edit `config.ini`

Download and install:
```
git clone https://github.com/bookletchoir/i3wm-config ~/i3wm-config
cd ~/i3wm-config
./build.sh
```

### TODO
- [ ] fix unicode handler with regex
- [ ] use makefile and cython

![Screenshot](./screenshot.png)

