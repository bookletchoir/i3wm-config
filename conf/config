# Please see http://i3wm.org/docs/userguide.html for a complete reference!

# i3wm default configs {{{

# Mod keys {{{
set $mod Mod4
set $alt Mod1
set $shift Shift
set $ctrl Control
set $esc Escape
set $ret Return
set $del Delete
set $numdel KP_Decimal
set $numlock Num_Lock
set $capslock Caps_Lock
floating_modifier $mod

# }}}

# Font
#font pango:Ubuntu Mono derivative Powerline 10
font xft:Iosevka Slab Term 10

# Window border
new_window pixel 3

# Titlebar format
#for_window [class=".*"] title_format " + %title"

# Colors {{{
# class                 border  backgr. text    indicator child_border
client.focused          #222222 #333333 #ffffff #61afef   #285577
client.focused_inactive #4c7899 #555555 #ffffff #484e50   #285577
client.unfocused        #333333 #111111 #888888 #292d2e   #222222
client.urgent           #E74E40 #E74E40 #ffffff #DD000A   #E74E40
#client.placeholder      #000000 #0c0c0c #ffffff #000000   #0c0c0c

client.background       #ffffff

# }}}

# Workspace name {{{
set $ws1 1: 
set $ws2 2: 
set $ws3 3: 
set $ws4 4: 
set $ws5 5: 
set $ws6 6: 
set $ws7 7: 
set $ws8 8: 
set $ws9 9: 
set $ws10 10: 

# }}}

# Status bar {{{
bar {
    status_command i3blocks -c "$HOME/.config/i3/i3blocks.conf"

    position top
    font xft:Misc Ohsnap 9

    colors {
        background #000000
        statusline #ffffff
        separator #666666

        focused_workspace  #4c7899 #285577 #ffffff
        active_workspace   #333333 #5f676a #ffffff
        inactive_workspace #222222 #222222 #888888
        urgent_workspace   #E74E40 #E74E40 #ffffff
        #binding_mode       #2f343a #900000 #ffffff
    }
}
# }}}

# }}}

# Keyboard binding {{{

# kill focused window
bindsym $mod+w kill

# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# move focused window
bindsym $mod+$shift+h move left
bindsym $mod+$shift+j move down
bindsym $mod+$shift+k move up
bindsym $mod+$shift+l move right

# split in horizontal orientation
bindsym $mod+backslash split h
bindsym $mod+c split h

# split in vertical orientation
bindsym $mod+minus split v
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+space layout toggle split
bindsym $mod+$alt+space layout tabbed
bindsym $mod+$shift+p layout stacking

# toggle tiling / floating
bindsym $mod+s floating toggle

# change focus between tiling / floating windows
#bindsym $mod+space focus mode_toggle

# focus the parent container
#bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# switch to workspace
bindsym $mod+1 workspace $ws1
bindsym $mod+2 workspace $ws2
bindsym $mod+3 workspace $ws3
bindsym $mod+4 workspace $ws4
bindsym $mod+5 workspace $ws5
bindsym $mod+6 workspace $ws6
bindsym $mod+7 workspace $ws7
bindsym $mod+8 workspace $ws8
bindsym $mod+9 workspace $ws9
bindsym $mod+0 workspace $ws10

bindsym $mod+n workspace next
bindsym $mod+p workspace prev
bindsym $mod+Tab workspace back_and_forth

# move focused container to workspace
bindsym $mod+$shift+1 move container to workspace $ws1
bindsym $mod+$shift+2 move container to workspace $ws2
bindsym $mod+$shift+3 move container to workspace $ws3
bindsym $mod+$shift+4 move container to workspace $ws4
bindsym $mod+$shift+5 move container to workspace $ws5
bindsym $mod+$shift+6 move container to workspace $ws6
bindsym $mod+$shift+7 move container to workspace $ws7
bindsym $mod+$shift+8 move container to workspace $ws8
bindsym $mod+$shift+9 move container to workspace $ws9
bindsym $mod+$shift+0 move container to workspace $ws10

# reload the configuration file
bindsym $mod+$esc exec i3-msg reload && notify-send "i3wm" "Configuration reloaded." || notify-send "i3wm" "Configuration failed to reload."
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+$shift+r restart

hide_edge_borders none

focus_follows_mouse no

# resize window (you can also use the mouse for that)
mode "resize" {
        bindsym h resize shrink width 10 px or 10 ppt
        bindsym j resize grow height 10 px or 10 ppt
        bindsym k resize shrink height 10 px or 10 ppt
        bindsym l resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

bindsym $mod+r mode "resize"

# }}}

# Applications rule  {{{
# assign param: instance class title

###################
# Workspace Shell #
###################

#################
# Workspace Web #
#################

assign [class="^Firefox$"] $ws2
for_window [class="^Firefox$" instance="Toplevel"] floating enable
for_window [class="^Firefox$" title="^About Mozilla Firefox$"] floating enable
for_window [class="^Firefox$" title="^Page Info - (.)*"] floating enable

assign [class="^Thunderbird$"] $ws2
for_window [class="^Thunderbird$" title="^Thunderbird Preferences$"] floating enable

####################
# Workspace Public #
####################

assign [class="Telegram"] $ws3
assign [class="Rambox"] $ws3

###################
# Workspace Media #
###################

assign [class="smplayer"] $ws4
assign [class="cantata"] $ws4

#########################
# Workspace Development #
#########################

assign [class="^FirefoxDeveloper$"] $ws5
for_window [class="^FirefoxDeveloper$" instance="Toplevel"] floating enable

assign [class="^qtcreator-bin$"] $ws5
assign [class="^QtCreator$"] $ws5

assign [class="Unity"] $ws5
for_window [class="Unity" title="^Starting Unity$"] floating enable
for_window [class="Unity" title="^Importing Package$"] floating enable
for_window [class="Unity" title="^Hold On$"] floating enable
assign [class="MonoDevelop"] $ws5
assign [class="jetbrains-idea"] $ws5
assign [class="jetbrains-clion"] $ws5
assign [class="jetbrains-rider"] $ws5
assign [class="jetbrains-pycharm"] $ws5
assign [class="com-intellij-updater-Runner"] $ws5
for_window [class="com-intellij-updater-Runner"] floating enable
assign [title="^Visual Paradigm Community Edition$"] $ws5
for_window [class="Genymotion Player"] floating enable
assign [class="Genymotion"] $ws5
assign [class="RPG Maker MV"] $ws5

##################
# Workspace File #
##################

assign [class="Nemo"] $ws6
for_window [class="Nemo" title="^Plugins$"] floating enable
for_window [class="Nemo" title="^Edit Bookmarks$"] floating enable

##################
# Workspace Game #
##################

assign [class="^Steam$"] $ws7
assign [title="^Steam$"] $ws7
assign [class="^Zenity$" instance="zenity" title="^Progress$"] $ws7
assign [class="^Steam$" title="^Steam - Uninstall$"] $ws7
assign [class="^Steam$" title="^Move Install Folder$"] $ws7
for_window [class="^Steam$" title="^$"] floating enable
for_window [class="^Steam$" title="^Create or select new Steam library folder:$"] floating enable
for_window [class="^steam$" title="^Steam Controller Configurator$"] floating enable
for_window [class="^Steam$" title="^Add a game$"] floating enable
for_window [class="^Steam$" title="^Friends$"] floating enable
for_window [class="^Steam$" title="^Steam - News$"] floating enable
for_window [class="^Steam$" title=".* - Chat$"] floating enable
for_window [class="^Steam$" title="^Settings$"] floating enable
for_window [class="^Steam$" title=".* - event started$"] floating enable
for_window [class="^Steam$" title=".* CD key$"] floating enable
for_window [class="^Steam$" title="Steam - Self Updater$"] floating enable
for_window [class="^Steam$" title="^Screenshot Uploader$"] floating enable
for_window [class="^Steam$" title="^Steam Guard - Computer Authorization Required$"] floating enable
for_window [class="^Steam$" title="^Steam Library Folders$"] floating enable
for_window [class="^Steam$" title="^Select external screenshots folder$"] floating enable
for_window [title="^Steam Keyboard$"] floating enable

assign [class="^ppsspp$"] $ws7
for_window [class="^ppsspp$"] floating enable

assign [class="^love$" instance="love" title="^FakeSG$"] $ws7
for_window [class="^retroarch$"] floating enable
assign [class="^retroarch$"] $ws7

for_window [class="^Desmume$"] floating enable
assign [class="^Desmume$"] $ws7

for_window [class="^mGBA$"] floating enable
assign [class="^mGBA$"] $ws7

assign [class="^Wine$" instance="^fakesg.exe$"] $ws7

##################
# Workspace Sync #
##################

assign [class="Deluge"] $ws8
assign [class="Uget-gtk"] $ws8

######################
# Workspace Document #
######################

assign [instance="^libreoffice$"] $ws9
assign [title="^LibreOffice$"] $ws9
#assign [class="^Soffice$"] $ws9
assign [class="^Soffice$" title="^Text Import"] $ws9
for_window [class="^Soffice$" title="^Text Import"] floating enable
assign [class="^Zathura$"] $ws9

##################
# Workspace Misc #
##################

assign [class="^Gimp$"] $ws10
assign [class="^Gimp-2.8$"] $ws10
assign [class="^VirtualBox$"] $ws10

###############
# Other rules #
###############


for_window [class="^Catfish$"] floating enable
for_window [class="^Fsearch$"] floating enable
for_window [class="^Gnome-system-monitor$"] floating enable
for_window [class="^Pinentry$"] floating enable
for_window [class="^Wine$" title="^Program Error Details$"] floating enable
for_window [class="^com-intellij-rt-execution-application-AppMain$"] floating enable
for_window [class="^copyq$"] floating enable
for_window [title="^Pick$"] floating enable

for_window [class="^Crossover$" title="^CrossOver License$"] floating enable
for_window [class="^Crossover$" title="^CrossOver Software Installer$"] floating enable
for_window [class="^Crossover$" title="^Menus in (.)*"] floating enable
for_window [class="^Crossover$" title="^Plugins in (.)*"] floating enable
for_window [class="^Crossover$" title="^Run Command$"] floating enable
for_window [class="^Crossover$" title="^System Information$"] floating enable
for_window [class="^Wine$" title="^Task Manager$"] floating enable

for_window [class="^Seahorse-tool$" title="^Encrypt Multiple Files$"] floating enable

for_window [instance="Sword Girls Online Simulator.exe" class="Wine"] floating enable
# }}}

# Applications binding {{{

# Exit i3wm
bindsym $ctrl+$alt+$del exec i3shutdown
bindsym $ctrl+$alt+$numdel exec i3shutdown

# searching util
bindsym $mod+slash exec catfish
#bindsym $mod+slash exec fsearch

# System monitor
bindsym $ctrl+$esc exec gnome-system-monitor

# Terminal
bindsym $mod+$ret exec termite -e "tmux-session Blanc"
bindsym $mod+$shift+$ret exec termite -e "tmux-session Arca"
bindsym $mod+$ctrl+$ret exec termite -e "tmux-session Ellen"
bindsym $mod+$alt+$ret exec termite -e "tmux-session Noir"

# program launcher
bindsym $alt+F1 exec rofi -show window
bindsym $alt+F2 exec rofi -show run

# file manager
bindsym $mod+e exec nemo

# screen lock
#bindsym $mod+F11 exec cinnamon-screensaver-command -l

# screenshot
bindsym Print exec printscn -m full
#bindsym $ctrl+Print exec printscn -m full -u
bindsym $shift+Print exec printscn -m select
#bindsym $ctrl+$shift+Print exec printscn -m select -u
bindsym $ctrl+Print exec printscn -m window
#bindsym $ctrl+$alt+Print exec printscn -m window -u

# clipboard
bindsym $ctrl+semicolon exec copyq toggle

# lock screen
bindsym $mod+F12 exec xautolock -locknow

# media keys {{{

# asus notebook fn keys

# fn-1
bindsym XF86Sleep exec systemctl hybird-sleep

# fn-{3,4}
bindsym XF86KbdBrightnessUp exec dkeyboardled up
bindsym XF86KbdBrightnessDown exec dkeyboardled down

# fn-{5,6}
bindsym XF86MonBrightnessUp exec dbrightness up
bindsym XF86MonBrightnessDown exec dbrightness down

# fn-9
bindsym XF86TouchpadToggle exec touchpad-script toggle

# fn-{10,11,12}
bindsym XF86AudioMute exec amixer set Master toggle && pkill -RTMIN+10 i3blocks
bindsym XF86AudioRaiseVolume exec amixer set Master on && amixer set Master 5%+ && pkill -RTMIN+10 i3blocks
bindsym XF86AudioLowerVolume exec amixer set Master on && amixer set Master 5%- && pkill -RTMIN+10 i3blocks

# fn-{up,down,left,right}
bindsym XF86AudioPlay exec mpdcontrol toggle && pkill -RTMIN+10 i3blocks
bindsym XF86AudioStop exec mpdcontrol stop && pkill -RTMIN+10 i3blocks
bindsym XF86AudioNext exec mpdcontrol next && pkill -RTMIN+10 i3blocks
bindsym XF86AudioPrev exec mpdcontrol prev && pkill -RTMIN+10 i3blocks

# fn-return
bindsym XF86Calculator exec speedcrunch

# fn-{mail,home}
bindsym XF86Mail exec thunderbird
bindsym XF86HomePage exec firefox

bindsym XF86AudioMedia exec cantata

# }}}

# }}}

# Signal binding {{{
bindsym $numlock exec pkill -RTMIN+11 i3blocks
bindsym $capslock exec pkill -RTMIN+11 i3blocks

# }}}

exec source $HOME/.config/autostartrc
