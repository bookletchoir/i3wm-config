#!/usr/bin/env python3

import subprocess

import utils


def mpd():
    conf = utils.read_conf("mpd")
    color = conf.get("color", "000000")

    try:
        mpc_output = subprocess.check_output(
            [
                conf.get("mpc_path", "mpc"),
                "--format", conf.get("format", "%title - %artist"),
                "--host", conf.get("host", "localhost"),
                "--port", conf.get("port", "6000")
            ],
            timeout=conf.getint("timeout", 1)
        )
        mpd_status = mpc_output.decode().strip().split("\n")

        if len(mpd_status) == 3:  # Playing or paused
            label = mpd_status[0]

            if "playing" in mpd_status[1]:
                emblem = conf.get("emblem_playing", "")
            elif "paused" in mpd_status[1]:
                emblem = conf.get("emblem_paused", "")
            else:  # Unexpected case
                emblem = conf.get("emblem_default", "")
        else:  # Stopped
            emblem = conf.get("emblem_default", "")
            label = "N.A"
    except (subprocess.CalledProcessError, FileNotFoundError):
        emblem = conf.get("emblem_default", "")
        label = "N.A"

    if conf.getboolean("uppercase", False):
        label = label.upper()

    return utils.print_u(emblem, label, color)


if __name__ == '__main__':
    print(mpd())
