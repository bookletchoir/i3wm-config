#!/usr/bin/env python3

import time
import psutil

import utils


def disk_io():
    conf = utils.read_conf("diskio")
    emblem = conf.get("emblem", "")
    color = conf.get("color", "000000")

    # get raw disk io
    pre_io = psutil.disk_io_counters()
    time.sleep(conf.getint("io_interval", 1))
    pos_io = psutil.disk_io_counters()

    # calc net disk io time
    net_read_time = pos_io[4] - pre_io[4]
    net_write_time = pos_io[5] - pre_io[5]

    try:
        disk_read_bytes = (pos_io[2] - pre_io[2]) * (1 / net_read_time)
        disk_read = utils.convert_unit(disk_read_bytes)
    except ZeroDivisionError:
        disk_read = "0B"

    try:
        disk_write_bytes = (pos_io[3] - pre_io[3]) * (1 / net_write_time)
        disk_write = utils.convert_unit(disk_write_bytes)
    except ZeroDivisionError:
        disk_write = "0B"

    label = '{2}{0: >6} {3}{1: >6}'.format(disk_read, disk_write,
                                           conf.get("sym_up", ""),
                                           conf.get("sym_down", ""))

    if conf.getboolean("uppercase", False):
        label = label.upper()

    return utils.print_u(emblem, label, color)


if __name__ == '__main__':
    print(disk_io())
