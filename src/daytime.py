#!/usr/bin/env python3

import time
import utils


def daytime():
    conf = utils.read_conf("daytime")
    color = conf.get("color", "000000")

    label = time.strftime(conf.get("time_format", "%y:%m:%d %H:%M"))

    if conf.getboolean("use_day_symbol", False):
        from datetime import datetime
        symbol = conf.get("sym_d{}".format(datetime.today().weekday()), "")

        if symbol:
            label = "{0} {1}".format(symbol, label)

    if conf.getboolean("dynamic_emblem", False):
        hour = int(time.strftime("%H"))
        emblem = conf.get("emblem_{}".format(hour), "")
    else:
        emblem = conf.get("emblem_default", "")

    if conf.getboolean("uppercase", False):
        label = label.upper()

    return utils.print_u(emblem, label, color)


if __name__ == '__main__':
    print(daytime())
