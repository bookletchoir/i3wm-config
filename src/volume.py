#!/usr/bin/env python3

import re
import subprocess

import utils


def volume():
    conf = utils.read_conf("volume")
    color = conf.get("color", "FFFFFF")
    color_inactive = conf.get("color_inactive", "000000")
    emblem = conf.get("emblem_default")
    label = "N.A"

    try:
        output_raw = subprocess.check_output(
            [
                conf.get("amixer_path", "amixer"),
                "get", "Master"
            ],
            timeout=conf.getint("timeout", 1)
        )

        if re.search('\[on\]',  output_raw.decode()):  # volume is on
            re_output = re.search('(?<=\[)\d+(?=%\])', output_raw.decode())
            vol_percent = int(re_output.group(0))

            if conf.getboolean("dynamic_emblem", False):
                if vol_percent >= 65:
                    emblem = conf.get("emblem_high", "")
                elif vol_percent >= 35:
                    emblem = conf.get("emblem_medium", "")
                else:
                    emblem = conf.get("emblem_low", "")
            label = "{0}%".format(vol_percent) if vol_percent else "N.A"
        else:  # volume if off
            color = color_inactive
            label = "Off"
            emblem = conf.get("emblem_muted", "")

    except (subprocess.CalledProcessError, subprocess.CalledProcessError,
            re.error, ValueError, Exception):
        color = color_inactive

    if conf.getboolean("uppercase", False):
        label = label.upper()

    return utils.print_u(emblem, label, color)


if __name__ == '__main__':
    print(volume())
