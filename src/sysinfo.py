#!/usr/bin/env python3

import re
import subprocess

import utils


def sysinfo(show_emblem=True, short=False, timeout=1):
    conf = utils.read_conf("sysinfo")
    emblem = conf.get("emblem", "")
    color = conf.get("color", "000000")

    label = ""

    if conf.getboolean("show_os_name", True):
        os_name = ""

        try:
            os_release = open('/etc/os-release').read()
            os_name = re.search('(?<=NAME=").*(?=")', os_release).group(0)
        except (FileNotFoundError, FileExistsError, re.error, Exception):
            pass

        label += os_name if os_name else ""

    if conf.getboolean("show_kernel_ver", True):
        label += " " if label else ""  # add space between values

        kernel_ver = ""

        try:
            output_raw = subprocess.check_output(
                ["uname", "-r"],
                timeout=conf.getint("timeout", 1)
            )
            kernel_ver = output_raw.decode().strip()
        except (subprocess.CalledProcessError, subprocess.TimeoutExpired,
                Exception):
            pass

        label += kernel_ver if kernel_ver else ""

    if conf.getboolean("uppercase", False):
        label = label.upper()

    return utils.print_u(emblem, label, color)


if __name__ == '__main__':
    print(sysinfo())
