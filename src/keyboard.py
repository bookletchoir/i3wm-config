#!/usr/bin/env python3

import re
import subprocess

import utils


def keyboard():
    conf = utils.read_conf("keyboard")
    emblem = conf.get("emblem", "")
    sym_numlock = conf.get("sym_numlock", "1")
    sym_capslock = conf.get("sym_capslock", "A")
    color = conf.get("color", "000000")
    color_active = conf.get("color_active", "000000")
    color_inactive = conf.get("color_inactive", "000000")

    label = ''
    xkb_status = subprocess.check_output(
        "xset q".split(), timeout=conf.getint("timeout", 1)
    )

    numlock_status = re.search('(Num Lock: +)(on|off)', xkb_status.decode())
    capslock_status = re.search('(Caps Lock: +)(on|off)', xkb_status.decode())

    if numlock_status.group(2) == "on":
        color_numlock = color_active
    else:
        color_numlock = color_inactive

    if capslock_status.group(2) == "on":
        color_capslock = color_active
    else:
        color_capslock = color_inactive

    if conf.getboolean("uppercase", False):
        sym_numlock = sym_numlock.upper()
        sym_capslock = sym_capslock.upper()

    label = "{0}{1}".format(
        utils.colorize(sym_numlock, color_numlock),
        utils.colorize(sym_capslock, color_capslock)
    )

    return utils.print_u(emblem, label, color)


if __name__ == '__main__':
    print(keyboard())
