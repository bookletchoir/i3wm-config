#!/usr/bin/env python3

import re
import subprocess

import utils


def battery():
    conf = utils.read_conf('battery')
    color = conf.get("color", "000000")

    # Read acpi output
    acpi_status = subprocess.check_output(
        'acpi -b'.split(), timeout=conf.getint("timeout", 1)
    ).strip().decode()

    if acpi_status:  # Output available (using battery)
        # Get status: (unknown, charging, discharging, full)
        re_status = re.search('(?<=: )\w+(?=,)', acpi_status)
        status = re_status.group()
        # Get percent
        re_percent = re.search('(?<=, )\d+(?=%)', acpi_status)

        percent = "{}".format(re_percent.group())

        if status == 'Unknown':  # Charged, Plugged in
            emblem = conf.get('emblem_plugged', "")
            label = "{}%".format(percent)
        elif status == 'Full':  # Full
            emblem = conf.get('emblem_full', '')
            label = 'Full'
        else:  # Charging or discharged
            if conf.getboolean("dynamic_emblem", False):
                if int(percent) > 80:  # 81~100
                    emblem = conf.get("sym_battery_full", "")
                elif int(percent) > 60:  # 61~80
                    emblem = conf.get("sym_battery_three_quarters", "")
                elif int(percent) > 40:  # 41~60
                    emblem = conf.get("sym_battery_half", "")
                elif int(percent) > 20:  # 21~40
                    emblem = conf.get("sym_battery_quarter", "")
                else:  # 0~20
                    emblem = conf.get("sym_battery_empty", "")
            else:
                emblem = conf.get('emblem_normal', '')

            label = "{}%".format(percent)

            if status == "Charging":
                label = "+{}".format(label)
    else:  # Not using battery
        emblem = conf.get('emblem_plugged', "")
        label = "A.C."

    if conf.getboolean("uppercase", False):
        label = label.upper()

    return utils.print_u(emblem, label, color)


if __name__ == '__main__':
    print(battery())
