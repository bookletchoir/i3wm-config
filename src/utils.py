#!/usr/bin/env python3

import configparser
import subprocess
import os
import re
from itertools import chain

F_CONF = "config.ini"

HOME = os.path.expanduser("~")
XDG_CONFIG_DIR = os.environ.get("XDG_CONFIG_DIR", os.path.join(HOME, ".config"))

CONFIG_FILES = [
    os.environ.get("BLOCKLETS_CONFIG_FILE", ""),
    os.path.join(os.getcwd(), F_CONF),
    os.path.join(XDG_CONFIG_DIR, "i3", "blocklets", F_CONF),
    os.path.join(XDG_CONFIG_DIR, "i3", F_CONF)
]


def read_conf(module_name, f_conf=""):
    if not f_conf:
        for file in CONFIG_FILES:
            if os.path.isfile(file):
                f_conf = file
                break

    if not f_conf:
        raise Exception("ERROR: Config file not found.")

    config = configparser.ConfigParser()
    config._interpolation = configparser.ExtendedInterpolation()
    config.read(f_conf)

    return config[module_name]


def pidof(proc_name, timeout=1):
    try:
        return subprocess.check_output(
            ["pidof", proc_name], timeout=timeout
        ).decode().strip()
    except subprocess.CalledProcessError:
        return None


def convert_unit(value, unit_size=1024):
    result = "0B"

    for s in ['B', 'K', 'M', 'G', 'T', 'P']:
        if value < unit_size:
            result = "{0:.1f}{1}".format(value, s)
            break
        else:
            value /= unit_size

    return result


def convert_temp(value, unit_src, unit_dst):
    if unit_src == "C" and unit_dst == "F":
        value = value * 9 / 5 + 32
    elif unit_src == "C" and unit_dst == "K":
        value += 273.15
    elif unit_src == "F" and unit_dst == "C":
        value = (value - 32) / (9 / 5)
    elif unit_src == "F" and unit_dst == "K":
        value = (value + 459.67) * 5 / 9
    elif unit_src == "K" and unit_dst == "C":
        value -= 273.15
    elif unit_src == "K" and unit_dst == "F":
        value = value * 9 / 5 - 459.67

    return value


def colorize(text, color):
    return '<span color="#{1}">{0}</span>'.format(text, color)


def print_u(emblem="", label="", color="000000", escape_unicode=True):
    sep = " " if emblem and label else ""
    text = "{0}{2}{1}".format(emblem, label, sep)

    return _print_u(text, color, escape_unicode)


def _print_u(text, color, escape_unicode):
    # TODO fix with regex replace

    if escape_unicode:
        pattern = r"(\\u\w{4})|(\\U\w{8})|(\\x\w{2})"
        re_matched = re.findall(pattern, text)
        matched_list = chain.from_iterable(re_matched)

        # temporary hack
        for u in matched_list:
            if u:
                # text = re.sub(u, u.encode().decode("unicode-escape"), text)
                text = text.replace(u, u.encode().decode("unicode-escape"))

                # text = text.encode().decode("unicode-escape")
                pass

    return colorize(text, color)
