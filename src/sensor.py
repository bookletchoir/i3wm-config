#!/usr/bin/env python3

import os
import re
import subprocess

import utils


def sensor():
    conf = utils.read_conf("sensor")
    emblem = conf.get("emblem", "")
    color = conf.get("color", "000000")
    color_inactive = conf.get("color_inactive", "000000")

    label = ""
    sym_degree = conf.get("sym_degree", "\\xb0")
    unit_default = "C"
    unit_target = conf.get("unit", "C")
    suffix = "{0}{1}".format(sym_degree, unit_target)

    null_val = "N.A"
    temp_vals = []

    if conf.getboolean("show_cpu_sensor", True):
        cpu_value = ""

        # read all `cpu_sensor` variables from config
        for key in conf.keys():
            if not key.startswith("cpu_sensor"):
                continue

            fpath = conf.get(key, "")

            if not os.path.isfile(fpath):
                continue

            try:
                cpu_value = int(open(fpath).read())
                break
            except (FileExistsError, ValueError, Exception):
                continue

        temp_vals.append(
            "{0:.0f}{1}".format(
                utils.convert_temp(cpu_value / 1000, unit_default, unit_target),
                suffix
            ) if cpu_value else null_val
        )

    if conf.getboolean("show_gpu_sensor", False):
        gpu_type = conf.get("gpu_type", "")
        gpu_value = ""

        if gpu_type == "nvidia":
            try:
                output_raw = subprocess.check_output(
                    [
                        conf.get("nvidia-smi_path", "nvidia-smi"),
                        "-q", "-d", "temperature"
                    ],
                    timeout=conf.getint("timeout", 1)
                )

                re_output = re.search('(?<=GPU Current Temp).*(\d+)(?= C)',
                                      output_raw.decode())
                gpu_value = re.search('\d+', re_output.group(0)).group(0)
            except (subprocess.TimeoutExpired, subprocess.CalledProcessError,
                    re.error, Exception):
                pass
        else:
            pass

        temp_vals.append(
            "{0:.0f}{1}".format(
                utils.convert_temp(gpu_value, unit_default, unit_target),
                suffix
            ) if gpu_value else null_val
        )

    if conf.getboolean("show_hdd_sensor", False):
        hdd_value = ""

        try:
            output_raw = subprocess.check_output(
                [
                    conf.get("telnet_path", "telnet"),
                    conf.get("hddtemp_host", "localhost"),
                    conf.get("hddtemp_port", "7634")
                ],
                timeout=conf.getint("timeout", 1)
            )
            re_output = re.search('(?<=\|)\d+(?=\|C)', output_raw.decode())
            hdd_value = re_output.group(0)
        except (subprocess.TimeoutExpired, subprocess.CalledProcessError,
                re.error, Exception):
            pass

        temp_vals.append(
            "{0:.0f}{1}".format(
                utils.convert_temp(hdd_value, unit_default, unit_target),
                suffix
            ) if hdd_value else null_val
        )

    # build label
    for val in temp_vals:
        _off = True if val == null_val else False  # Workaround for uppercase

        if conf.getboolean("uppercase", False):
            val = val.upper()

        val = utils.colorize(val, color_inactive) if _off else val
        label += " {0}".format(val) if label else val  # add space between vals

    return utils.print_u(emblem, label, color)


if __name__ == '__main__':
    print(sensor())
