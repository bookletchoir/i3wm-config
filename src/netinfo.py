#!/usr/bin/env python3

import os
import re
import subprocess

import utils


def netinfo():
    conf = utils.read_conf("netinfo")
    color = conf.get("color", "000000")
    label = "No Connections"

    timeout = conf.getint("timeout", 1)

    try:
        network_device = subprocess.check_output(
            'nmcli -t -f device,type,state,connection device'.split(),
            timeout=timeout
        )
        network_re = re.search('.*(ethernet|wifi).*:connected:.*',
                               network_device.decode())

        nm_device, nm_type, nm_state, nm_connection = \
            network_re.group(0).strip().split(":")
        label = nm_connection
    except (subprocess.CalledProcessError, FileNotFoundError, AttributeError):
        nm_type = None

    if nm_type:
        if nm_type.strip() == "ethernet":
            emblem = conf.get("emblem_wired", "")
        elif nm_type.strip() == "wifi":
            emblem = conf.get("emblem_wireless", "")
        else:
            emblem = conf.get("emblem_unknown", "")
    else:
        emblem = conf.get("emblem_unknown", "")
        color = conf.get("color_inactive", "000000")

    if conf.getboolean("uppercase", False):
        label = label.upper()

    if conf.getboolean("show_vpn_indicator", False) and nm_type:
        vpn_cmds = conf.get("vpn_cmds", "").split(" ")

        vpn_pids = []

        for cmd in vpn_cmds:
            vpn_pids.append(utils.pidof(cmd, timeout))

        if any(vpn_pids):
            emblem = conf.get("emblem_vpn", "")

        hide_inactive = conf.getboolean("hide_inactive_vpn", True)

        vpn_label = ""

        for i, cmd in enumerate(vpn_cmds):
            _pid = vpn_pids[i]
            _sym = conf.get("{0}_{1}".format("vpn_sym", cmd), cmd)

            if conf.get("uppercase_vpn_sym", False):
                _sym = _sym.upper()

            _color_active = conf.get(
                "{0}_{1}".format("color_active", cmd),
                conf.get("color_active", "FFFFFF")
            )
            _color_inactive = conf.get(
                "{0}_{1}".format("color_inactive", cmd),
                conf.get("color_inactive", "000000")
            )

            _color = _color_active if _pid else _color_inactive

            _sep = conf.get("vpn_separator_sym", "")

            if not (hide_inactive and not _pid):
                vpn_label += _sep if vpn_label else ""
                vpn_label += utils.colorize(_sym, _color)

        if vpn_label:
            _prefix = conf.get("vpn_sym_prefix", "")
            _suffix = conf.get("vpn_sym_suffix", "")

            vpn_label = "{1}{0}{2}".format(vpn_label, _prefix, _suffix)

            label = "{1} {0}".format(vpn_label, label)

    return utils.print_u(emblem, label, color)


if __name__ == '__main__':
    print(netinfo())
