#!/usr/bin/env python3

import time
import psutil

import utils


def netio():
    conf = utils.read_conf("netio")
    emblem = conf.get("emblem", "")
    color = conf.get("color", "")

    try:
        time_ratio = 1 / conf.getint("io_interval", 1)
    except ZeroDivisionError:
        time_ratio = 1

    # get raw net io
    pre_io = psutil.net_io_counters()
    time.sleep(conf.getint("io_interval", 1))
    pos_io = psutil.net_io_counters()

    # calculate net io
    net_sent_bytes = pos_io[0] - pre_io[0]
    net_recv_bytes = pos_io[1] - pre_io[1]

    # convert to higher unit
    net_sent = utils.convert_unit(net_sent_bytes * time_ratio)
    net_recv = utils.convert_unit(net_recv_bytes * time_ratio)

    if conf.getboolean("uppercase", False):
        net_sent = net_sent.upper()
        net_recv = net_recv.upper()

    label = '{2}{0: >6} {3}{1: >6}'.format(net_sent, net_recv,
                                           conf.get("sym_up", ""),
                                           conf.get("sym_down", ""))

    return utils.print_u(emblem, label, color)


if __name__ == '__main__':
    print(netio())
