#!/usr/bin/env python3

import utils


def separator():
    conf = utils.read_conf("separator")
    emblem = conf.get("emblem", "")
    color = conf.get("color", "000000")

    return utils.print_u(emblem, color=color)


if __name__ == '__main__':
    print(separator())
