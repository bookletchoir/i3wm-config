#!/usr/bin/env python3

import psutil

import utils


def memory():
    conf = utils.read_conf("memory")
    emblem = conf.get("emblem", "")
    color = conf.get("color", "000000")

    mem_raw = psutil.virtual_memory().total - psutil.virtual_memory().available
    label = utils.convert_unit(mem_raw)

    if conf.getboolean("show_swap", True):
        swap_raw = psutil.swap_memory().used
        label += " {0}".format(utils.convert_unit(swap_raw))

    # if conf.getboolean("uppercase", False):
    #     label = label.upper()

    return utils.print_u(emblem, label, color)


if __name__ == '__main__':
    print(memory())
