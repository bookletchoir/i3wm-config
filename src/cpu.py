#!/usr/bin/env python3

import os
import psutil

import utils


def cpu():
    conf = utils.read_conf("cpu")

    emblem = conf.get("emblem", "")
    color = conf.get("color", "000000")

    label = "{0:.1f}%".format(psutil.cpu_percent(conf.getint("io_interval", 1)))

    if conf.getboolean("show_cpu_avg", True):
        label += " {0:.1f}".format(os.getloadavg()[0])

    # if conf.getboolean("uppercase", False):
    #     label = label.upper()

    return utils.print_u(emblem, label, color)


if __name__ == '__main__':
    print(cpu())
